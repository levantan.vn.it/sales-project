<?php

namespace App\Http\Controllers;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

class DashboardController extends Controller
{
    public function home()
    {
        return view('frontend.dashboard.home');
    }

    public function dashboard()
    {
        return view('frontend.dashboard.dashboard');
    }
    // Product
    public function product()
    {
        return view('frontend.products.index');
    }
    public function productSearch()
    {
        return view('frontend.products.search');
    }
    public function productDetail()
    {
        return view('frontend.products.detail');
    }
    public function productCreate()
    {
        return view('frontend.products.create');
    }

    // Branch
    public function branch()
    {
        return view('frontend.branch.index');
    }

    // Promotion 
    public function promotion()
    {
        return view('frontend.promotion.index');
    }
    public function promotionDetail()
    {
        return view('frontend.promotion.detail');
    }
}
