<?php

namespace App\Http\Controllers\Admin;

use App\Forms\UserAdminForm;
use App\Http\Controllers\Controller;
use App\Http\Responses\BaseHttpResponse;
use App\Requests\CreateAdminRequest;
use App\Services\AdminService;
use App\Services\UsersDataTable;
use Kris\LaravelFormBuilder\FormBuilder;

class AdminUserController extends Controller
{

    protected $adminService;

    public function __construct(AdminService $adminService)
    {
        $this->adminService = $adminService;
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(UsersDataTable $dataTable)
    {
        if (request()->ajax()) {
            return $dataTable->ajaxData();
        }

        return $dataTable->render('admin.user.index');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create(FormBuilder $formBuilder)
    {
        $form = $formBuilder->create(UserAdminForm::class, [
            'method' => 'POST',
            'url' => route('users.store')
        ]);

        return view('admin.user.create', compact('form'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(CreateAdminRequest $request, BaseHttpResponse $response)
    {
        $data = $request->input();
        $data['password'] = bcrypt($request->input('password'));

        $this->adminService->create($data);

        return $response
            ->setPreviousUrl(route('users.create'))
            ->setNextUrl(route('users.index'))
            ->setMessage('Created successfully');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id, FormBuilder $formBuilder)
    {
        $item = $this->adminService->find($id);

        $form = $formBuilder->create(UserAdminForm::class, [
            'model' => $item,
            'method' => 'PUT',
            'url' => route('users.update', $item->id)
        ]);

        return view('admin.user.create', compact('form'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(CreateAdminRequest $request, $id, BaseHttpResponse $response)
    {
        $item = $this->adminService->find($id);

        $data = $request->input();

        // check password
        $data['password'] = $item->password;
        if ($request->has('password')) {
            $data['password'] = bcrypt($request->input('password'));
        }

        $this->adminService->update($item, $data);

        return $response
            ->setPreviousUrl(route('users.edit', $id))
            ->setNextUrl(route('users.index'))
            ->setMessage('Updated successfully');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id, BaseHttpResponse $response)
    {
        try {
            $this->adminService->delete($id);
            return $response->setMessage('Deleted successfully');
        }
        catch (Exception $exception) {
            return $response
                ->setError()
                ->setMessage($exception->getMessage());
        }
    }
}
