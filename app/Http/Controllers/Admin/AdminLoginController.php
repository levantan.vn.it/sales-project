<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Http\Requests\AdminLoginRequest;
use Illuminate\Foundation\Auth\AuthenticatesUsers;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class AdminLoginController extends Controller
{
    use AuthenticatesUsers;

    protected $redirectTo = '/admin/dashboard';

    public function __construct()
    {
        $this->middleware('guest:admin')->except('logout');
    }

    public function guard()
    {
        return Auth::guard('admin');
    }

    public function showLoginForm(Request $request)
    {
        return view('admin.auth.login');
    }

    public function login(AdminLoginRequest $request)
    {
        //attempt to log the user in
        if (auth()->guard('admin')->attempt(['email' => $request->email, 'password' => $request->password], $request->remember)){
            //if successful, then redirect to their intended location
            return redirect('/admin/dashboard');
        }
        return back()->withInput($request->only('email','remember'))->withErrors(['email' => 'Email or password are wrong.']);
    }

    public function logout(Request $request)
    {
        $this->guard('admin')->logout();

        return $this->loggedOut($request) ?: redirect('admin/login');
    }
}
