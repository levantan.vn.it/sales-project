<?php


namespace App\Requests;

use Illuminate\Foundation\Http\FormRequest;

class CreateAdminRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        $arrRules = [
            'name'            => 'required|max:60|min:2',
            'email'                 => 'required|max:60|min:6|email|unique:admins',
            'password'              => 'required|min:6',
            'password_confirmation' => 'required|same:password',
        ];

        if ($this->method() == 'PUT') {
            $arrRules['email'] = 'required|max:60|min:6|email|unique:admins,email,'.$this->route('user');
            $input = $this->request->all();
            if (empty($input['password'])) {
                unset($arrRules['password']);
                unset($arrRules['password_confirmation']);
            }
        }

        return $arrRules;
    }
}
