<?php


namespace App\Services;

use App\Models\Admin;
use Yajra\DataTables\DataTables;
use Yajra\DataTables\Html\Button;
use Yajra\DataTables\Services\DataTable;
use Html;

class UsersDataTable extends DataTable
{
    protected function getColumns()
    {
        return [
            'id'   => [
                'name'  => 'id',
                'title' => 'Id',
                'class' => 'text-left',
            ],
            'name'   => [
                'name'  => 'name',
                'title' => 'Name',
                'class' => 'text-left',
            ],
            'email'   => [
                'name'  => 'email',
                'title' => 'Email',
                'class' => 'text-left',
            ],
            'created_at'   => [
                'name'  => 'created_at',
                'title' => 'Created At',
                'class' => 'text-left',
            ],
            'updated_at'   => [
                'name'  => 'updated_at',
                'title' => 'Updated At',
                'class' => 'text-left',
            ],
            'operations' => [
                'title'      => 'Operations',
                'width'      => '134px',
                'class'      => 'text-center',
                'orderable'  => false,
                'searchable' => false,
                'exportable' => false,
                'printable'  => false,
            ],
        ];
    }

    public function html()
    {
        return $this->builder()
            ->columns($this->getColumns())
            ->parameters([
                'dom'          => 'Bfrtip',
                'searching'    => true,
                'searchDelay'  => 350,
                'language'     => [
                    'search'            => '',
                    'searchPlaceholder' => 'Search...'
                ],
            ]);

    }

    public function ajaxData()
    {
        return DataTables::of(Admin::query())
            ->editColumn('created_at', function ($item) {
                return date('d-m-Y', strtotime($item->created_at) );
            })
            ->editColumn('updated_at', function ($item) {
                return date('d-m-Y', strtotime($item->updated_at) );
            })
            ->addColumn('operations', function ($item) {

                return view('admin.user.datatable.actions', ['item' => $item])->render();
            })
            ->escapeColumns([])
            ->make(true);
    }
}
