<?php


namespace App\Services;


use App\Models\Admin;

class AdminService
{
    protected $adminModel;

    public function __construct(Admin $admin)
    {
        $this->adminModel = $admin;
    }

    public function find($id)
    {
        return $this->adminModel::findOrFail($id);
    }

    public function create($data)
    {
        $this->adminModel->fill($data);
        $this->adminModel->save();
    }

    public function update($item, $data)
    {
        $item = $item->fill($data);
        $item->save();
    }

    public function delete($id)
    {
        return $this->adminModel::findOrFail($id)->delete();
    }
}
