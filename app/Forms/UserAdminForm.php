<?php


namespace App\Forms;

use App\Forms\Fields\HtmlField;
use App\Models\Admin;
use App\Requests\CreateAdminRequest;
use Kris\LaravelFormBuilder\Form;

class UserAdminForm extends Form
{

    public function addCustomField($name, $class)
    {
        parent::addCustomField($name, $class);

        return $this;
    }

    public function withCustomFields(): self
    {
        $customFields = [
            'html'         => HtmlField::class,
        ];

        foreach ($customFields as $key => $field) {
            if (!$this->formHelper->hasCustomField($key)) {
                $this->addCustomField($key, $field);
            }
        }

        return $this;
    }

    public function buildForm()
    {
        $item = $this->getModel();

        $this
            ->setupModel(new Admin())
//            ->setValidatorClass(CreateAdminRequest::class)
            ->withCustomFields()
            ->add('rowOpen1', 'html', [
                'html' => '<div class="row">',
            ])
            ->add('name', 'text', [
                'label'      => 'Name',
                'label_attr' => ['class' => 'control-label required'],
                'attr'       => [
                    'data-counter' => 30,
                ],
                'wrapper'    => [
                    'class' => $this->formHelper->getConfig('defaults.wrapper_class') . ' col-md-6',
                ],
                'value' => ($item) ? $item->name : NULL,
            ])
            ->add('rowClose1', 'html', [
                'html' => '</div>',
            ])
            ->add('rowOpen2', 'html', [
                'html' => '<div class="row">',
            ])
            ->add('email', 'text', [
                'label'      => 'Email',
                'label_attr' => ['class' => 'control-label required'],
                'attr'       => [
                    'placeholder'  => 'Ex: example@gmail.com',
                    'data-counter' => 60,
                ],
                'wrapper'    => [
                    'class' => $this->formHelper->getConfig('defaults.wrapper_class') . ' col-md-6',
                ],
                'value' => ($item) ? $item->email : NULL,
            ])
            ->add('rowClose2', 'html', [
                'html' => '</div>',
            ])
            ->add('rowOpen3', 'html', [
                'html' => '<div class="row">',
            ])
            ->add('password', 'password', [
                'label'      => 'Password',
                'label_attr' => ['class' => 'control-label required'],
                'attr'       => [
                    'data-counter' => 60,
                ],
                'wrapper'    => [
                    'class' => $this->formHelper->getConfig('defaults.wrapper_class') . ' col-md-6',
                ],
            ])
            ->add('password_confirmation', 'password', [
                'label'      => 'Re-type password',
                'label_attr' => ['class' => 'control-label required'],
                'attr'       => [
                    'data-counter' => 60,
                ],
                'wrapper'    => [
                    'class' => $this->formHelper->getConfig('defaults.wrapper_class') . ' col-md-6',
                ],
            ])
            ->add('rowClose3', 'html', [
                'html' => '</div>',
            ]);
    }
}
