@extends('layouts.frontend.app')

@section('content')
<style>
    @media only screen and (max-width:376px){
    .header{
        display: flex !important;
        margin: 0 !important;
        padding: 0 15px !important;
        background-color: #ffffff !important;
    }
    .header .left_header, .header .right_header{
        margin: 45px 0 0 !important;
        font-size: 24px !important;
        line-height: 32px !important;
    }
    .header .left_header .header_name{
        font-size: 24px !important;
    }
    .header .right_header .avatar{
        margin-left: 16px !important;
    }
    .header .right_header #bell .red{
        left: 93.89% !important;
        top: 7.33% !important;
    }
}
</style>
<div class="layout-d">
    <div class="colum row">
        <div class="colum_object pd-left col-md-3 col-6">
            <a href="{{route('dashboard')}}">
                <div class="color">
                    <div class="colum_object-img">
                        <img class="icon" src="{{asset('css/frontend/myimg/window1.png')}}" alt="">
                        <img class="icon mobile" src="{{asset('css/frontend/myimg/windowM.png')}}" alt="">
                    </div>
                    <div class="colum_object-title">
                        <h3>Dashboard</h3>
                    </div>
                </div>
            </a>
        </div>
        <div class="colum_object col-md-3 col-6">
            <a href="">
                <div class="color">
                    <div class="colum_object-img" id="ting">
                        <img class="icon" src="{{asset('css/frontend/myimg/box1.png')}}" alt="">
                        <img class="icon mobile" src="{{asset('css/frontend/myimg/productM.png')}}" alt="">
                        <button class="slave">4</button>
                    </div>
                    <div class="colum_object-title">
                        <h3>Orders</h3>
                    </div>
                </div>
            </a>
        </div>
        <div class="colum_object col-md-3 col-6">
            <a href="">
                <div class="color">
                    <div class="colum_object-img">
                        <img class="icon" src="{{asset('css/frontend/myimg/star1.png')}}" alt="">
                        <img class="icon mobile" src="{{asset('css/frontend/myimg/starM.png')}}" alt="">
                    </div>
                    <div class="colum_object-title">
                        <h3>Customers</h3>
                    </div>
                </div>
            </a>
        </div>
        <div class="colum_object pd-right col-md-3 col-6">
            <a href="{{route('product')}}">
                <div class="color">
                    <div class="colum_object-img">
                        <img class="icon" src="{{asset('css/frontend/myimg/chair1.png')}}" alt="">
                        <img class="icon mobile" src="{{asset('css/frontend/myimg/chairM.png')}}" alt="">
                    </div>
                    <div class="colum_object-title">
                        <h3> <span>Wished</span> Products</h3>
                    </div>
                </div>
            </a>
        </div>
        <div class="colum_object pd-left col-md-3 col-6">
            <a href="{{route('promotion')}}">
                <div class="color">
                    <div class="colum_object-img">
                        <img class="icon" src="{{asset('css/frontend/myimg/ticket1.png')}}" alt="">
                        <img class="icon mobile" src="{{asset('css/frontend/myimg/ticketM.png')}}" alt="">
                    </div>
                    <div class="colum_object-title">
                        <h3>Promotions</h3>
                    </div>
                </div>
            </a>
        </div>
        <div class="colum_object col-md-3 col-6">
            <a href="">
                <div class="color">
                    <div class="colum_object-img">
                        <img class="icon" src="{{asset('css/frontend/myimg/list1.png')}}" alt="">
                        <img class="icon mobile" src="{{asset('css/frontend/myimg/listM.png')}}" alt="">
                    </div>
                    <div class="colum_object-title">
                        <h3><span>Selling</span> Report</h3>
                    </div>
                </div>
            </a>
        </div>
        <div class="colum_object col-md-3 col-6">
            <a href="">
                <div class="color">
                    <div class="colum_object-img">
                        <img class="icon" src="{{asset('css/frontend/myimg/setting1.png')}}" alt="">
                        <img class="icon mobile" src="{{asset('css/frontend/myimg/settingM.png')}}" alt="">
                    </div>
                    <div class="colum_object-title">
                        <h3>Settings</h3>
                    </div>
                </div>
            </a>
        </div>
    </div>
</div>


@endsection

