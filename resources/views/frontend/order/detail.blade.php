@extends('layouts.frontend.app')
@section('content')
    <div class="column">
        <div class="col-md-12">
            <div class="column-header row">
                <a href="{{ url('/order') }}" class="btn btn-light btn-back"><img class="dot"
                        src="{{ asset('images/back-arrow.png') }}" alt=""></a>
                <div class="column-title">
                    #ORDERID
                </div>
                <button class="status">Status</button>
                <button type="button" class="btn btn-outline-light btn-ghost" href="#"><img src="{{ asset('images/locked-padlock.png') }}" alt=""></button>
                <button type="button" class="btn btn-outline-light btn-edit" href="#"><img src="{{ asset('images/draw.png') }}" alt=""></button>
                </a>
                <a href="" class="btn btn-outline-light btn-delete"><img src="{{ asset('images/tick.png') }}" alt=""></a>
            </div>
            <div class="row">
                <div class="col-lg-6">
                    <div class="customer">
                        <div class="customer-header">
                            Customer Information
                        </div>
                        <div class="customer-body">
                        </div>
                    </div>
                    <div class="promotion">
                        <div class="promotion-header">
                            Promotion Information
                        </div>
                        <div class="promotion-body">
                        </div>
                    </div>
                    <div class="promotion">
                        <div class="promotion-header">
                            Delivery Information
                        </div>
                        <div class="promotion-body">
                        </div>
                    </div>
                </div>
                <div class="col-lg-6">
                    <div class="product">
                        <div class="product-header">
                            Product List
                        </div>
                        <div class="product-body">
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
