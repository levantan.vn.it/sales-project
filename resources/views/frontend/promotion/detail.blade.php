@extends('layouts.frontend.app')
@section('content')
<style>
    .navFooter{display: none;}
</style>  
<div class="layout">
    <div class="title detail">
        <div class="back-title">
            <a href="{{route('promotion')}}">
                <button class="turn_back">
                    <img src="{{asset('css/frontend/myimg/angle.png')}}" alt="">
                </button>
            </a>
            <h1>Promotion Title</h1>
        </div> 
    </div>
    <div class="product_boxid row">
        <div class="box_site col-md-6 ">
            <div class="product_boxid_object col-md-12">
                <div class="object_title" style="height: 300px">
                    <h1>Promotion Information</h1>
                </div>
            </div>
            <div class="product_boxid_object col-md-12">
                <div class="object_title" style="height: 200px">
                    <h1>Promotion Condition</h1>
                </div>
            </div>
        </div>
        <div class="box_site col-md-6 ">
            <div class="product_boxid_object col-md-12">
                <div class="object_title" style="height: 510px">
                    <h1>Product Applied List</h1>
                </div>
            </div>
        </div>
    </div>
</div>
    
@endsection