@extends('layouts.frontend.app')
@section('content')
<style>
    .navFooter{display: none;}
</style>
<div class="layout ">
    <div class="title back" >
        <a href="{{route('product.search')}}">
            <button class="turn_back">
                <img src="{{asset('css/frontend/myimg/angle.png')}}" alt="">
            </button>
        </a>
        <h1>Brand Name</h1>
    </div>
    <form action="">
        <div class="search_form">
            <div class="search_box">
                <input type="text" name="" id="" placeholder="Search products">
                <a href="{{route('product.search')}}"><img src="{{asset('css/frontend/myimg/lock.png')}}" alt=""></a>
            </div>
            <button class="find"><img src="{{asset('css/frontend/myimg/caledar.png')}}" alt=""></button>
            <button class="find" id="gas1"><img src="{{asset('css/frontend/myimg/fill.png')}}" alt=""></button>
        </div>
    </form>
    <div class="product_box row">
        <div class="product_box_object col-md-4">
            <a href="{{route('product.detail')}}">
                <div class="object">
                    <h1>Product Item</h1>
                </div>
            </a>
        </div>
        <div class="product_box_object col-md-4">
            <a href="{{route('product.detail')}}">
                <div class="object">
                    <h1>Product Item</h1>
                </div>
            </a>
        </div>
        <div class="product_box_object col-md-4">
            <a href="{{route('product.detail')}}">
                <div class="object">
                    <h1>Product Item</h1>
                </div>
            </a>
        </div>
        <div class="product_box_object col-md-4">
            <a href="{{route('product.detail')}}">
                <div class="object">
                    <h1>Product Item</h1>
                </div>
            </a>
        </div>
        <div class="product_box_object col-md-4">
            <a href="{{route('product.detail')}}">
                <div class="object">
                    <h1>Product Item</h1>
                </div>
            </a>
        </div>
        <div class="product_box_object col-md-4">
            <a href="{{route('product.detail')}}">
                <div class="object">
                    <h1>Product Item</h1>
                </div>
            </a>
        </div><div class="product_box_object col-md-4">
            <a href="{{route('product.detail')}}">
                <div class="object">
                    <h1>Product Item</h1>
                </div>
            </a>
        </div>
        <div class="product_box_object col-md-4">
            <a href="{{route('product.detail')}}">
                <div class="object">
                    <h1>Product Item</h1>
                </div>
            </a>
        </div>
        <div class="product_box_object col-md-4">
            <a href="{{route('product.detail')}}">
                <div class="object">
                    <h1>Product Item</h1>
                </div>
            </a>
        </div>
    </div>
</div>
    
@endsection