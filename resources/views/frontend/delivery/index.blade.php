@extends('layouts.frontend.app')
@section('content')
    <div class="container">
        <div class="row mt-2">
            <div class="class-header col-md-12">
                <a href="{{ url('/cart') }}" class="cart-header">CARTS</a>
                <div class="vertical-line"></div>
                <a href="{{ url('/order') }}" class="order-header">ORDERS</a>
                <div class="line"></div>
                <a href="{{ url('/delivery') }}" class="delivery-header active">DELIVERY</a>
            </div>
            <div class="tab-content py-3 px-3 px-sm-0" id="nav-tabContent">
                <div class="tab-pane fade show active" id="nav-cart" role="tabpanel" aria-labelledby="nav-cart-tab">
                    <div class="input-group col-md-12">
                        <div class="search col-md-10 ml-2"><a href="#" class="search_icon"><i
                                    class="fa fa-search"></i></a><input class="search_input" type="text" name=""
                                placeholder="Search here..."></div>
                        <button class="btn-calender mr-3"><img class="dot" src="{{ asset('images/form-calendar_1.png') }}"
                                alt=""></button>
                        <button class="btn-calender mr-3"><img class="dot" src="{{ asset('images/down-arrow.png') }}"
                                alt=""></button>
                        <ul class="nav justify-content-center nav-pills mt-2">
                            <li class="nav-item">
                                <a class="nav-link active" href="#">All</a>
                            </li>
                            <li class="nav-item">
                                <a class="nav-link" href="#">Packing</a>
                            </li>
                            <li class="nav-item">
                                <a class="nav-link " href="#">Delivering</a>
                            </li>
                            <li class="nav-item">
                                <a class="nav-link " href="#">Delivered</a>
                            </li>
                            <li class="nav-item">
                                <a class="nav-link " href="#">Canceled</a>
                            </li>
                            <li class="nav-item">
                                <a class="nav-link " href="#">Completed</a>
                            </li>
                        </ul>
                        <div class="column row">
                            <div class="col-lg-6">
                                <div class="column_object">
                                    <div class="column_object-title">
                                        <a href="{{ url('/delivery/detail') }}">
                                            <h3>Delivery Item</h3>
                                        </a>
                                    </div>
                                </div>

                                <div class="column_object">
                                    <div class="column_object-title">
                                        <a href="{{ url('/delivery/detail') }}">
                                            <h3>Delivery Item</h3>
                                        </a>
                                    </div>
                                </div>
                                <div class="column_object">
                                    <div class="column_object-title">
                                        <a href="{{ url('/delivery/detail') }}">
                                            <h3>Delivery Item</h3>
                                        </a>
                                    </div>
                                </div>
                            </div>
                            <div class="col-lg-6">
                                <div class="column_object">
                                    <div class="column_object-title">
                                        <a href="{{ url('/delivery/detail') }}">
                                            <h3>Delivery Item</h3>
                                        </a>
                                    </div>
                                </div>
                                <div class="column_object">
                                    <div class="column_object-title">
                                        <a href="{{ url('/delivery/detail') }}">
                                            <h3>Delivery Item</h3>
                                        </a>
                                    </div>
                                </div>
                                <div class="column_object">
                                    <div class="column_object-title">
                                        <a href="{{ url('/delivery/detail') }}">
                                            <h3>Delivery Item</h3>
                                        </a>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
