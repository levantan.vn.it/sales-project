@extends('layouts.frontend.app')
@section('content')
    <div class="column">
        <div class="col-md-12">
            <div class="column-header row">
                <a href="{{ url('/delivery') }}" class="btn btn-light btn-back"><img
                        src="{{ asset('images/back-arrow.png') }}" alt=""></a>
                <div class="column-title">
                    #DELIVERYID
                </div>
                <button class="status">Status</button>
            </div>
            <div class="row">
                <div class="col-lg-6">
                    <div class="promotion">
                        <div class="promotion-header">
                            Receiver Information
                        </div>
                        <div class="promotion-body">
                        </div>
                    </div>
                    <div class="promotion">
                        <div class="promotion-header">
                            Delivery Information
                        </div>
                        <div class="promotion-body">
                        </div>
                    </div>
                    <div class="customer">
                        <div class="customer-header">
                            Customer Information
                        </div>
                        <div class="customer-body">
                        </div>
                    </div>
                </div>
                <div class="col-lg-6">
                    <div class="product">
                        <div class="product-header">
                            Delivery Lost
                        </div>
                        <div class="product-body">
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
