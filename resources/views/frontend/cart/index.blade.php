@extends('layouts.frontend.app')
@section('content')
    <div class="container">
        <div class="row mt-2">
            <div class="class-header col-md-12">
                <a href="{{ url('/cart') }}" class="cart-header active">CARTS</a>
                <div class="vertical-line"></div>
                <a href="{{ url('/order') }}" class="order-header">ORDERS</a>
                <div class="line"></div>
                <a href="{{ url('/delivery') }}" class="delivery-header">DELIVERY</a>
            </div>
            <div class="tab-content py-3 px-3 px-sm-0" id="nav-tabContent">
                <div class="tab-pane fade show active" id="nav-cart" role="tabpanel" aria-labelledby="nav-cart-tab">
                    <div class="input-group col-md-12">
                        <div class="search"><a href="#" class="search_icon"><i class="fa fa-search"></i></a><input
                                class="search_input" type="text" name="" placeholder="Search here..."></div>
                        <button class="btn-calender"><img class="dot" src="{{ asset('images/form-calendar_1.png') }}"
                                alt=""></button>
                        <button class="btn-cart">
                            <div class="btn-title">
                                Add New Cart
                            </div>
                        </button>
                        <div class="column row">
                            <div class="col-lg-6">
                                <div class="column_object">
                                    <div class="column_object-title">
                                        <a href="{{ url('/cart/detail') }}">
                                            <h3>Cart Item</h3>
                                        </a>
                                    </div>
                                </div>

                                <div class="column_object">
                                    <div class="column_object-title">
                                        <a href="{{ url('/cart/detail') }}">
                                            <h3>Cart Item</h3>
                                        </a>
                                    </div>
                                </div>
                                <div class="column_object">
                                    <div class="column_object-title">
                                        <a href="{{ url('/cart/detail') }}">
                                            <h3>Cart Item</h3>
                                        </a>
                                    </div>
                                </div>
                            </div>
                            <div class="col-lg-6">
                                <div class="column_object">
                                    <div class="column_object-title">
                                        <a href="{{ url('/cart/detail') }}">
                                            <h3>Cart Item</h3>
                                        </a>
                                    </div>
                                </div>
                                <div class="column_object">
                                    <div class="column_object-title">
                                        <a href="{{ url('/cart/detail') }}">
                                            <h3>Cart Item</h3>
                                        </a>
                                    </div>
                                </div>
                                <div class="column_object">
                                    <div class="column_object-title">
                                        <a href="{{ url('/cart/detail') }}">
                                            <h3>Cart Item</h3>
                                        </a>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
