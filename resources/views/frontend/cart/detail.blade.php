@extends('layouts.frontend.app')
@section('content')
    <div class="column">
        <div class="col-md-12">
            <div class="column-header row">
                <a href="{{ url('/cart') }}" class="btn btn-light btn-back"><img class="dot" src="{{ asset('images/back-arrow.png') }}" alt=""></a>
                <div class="column-title">
                    #CARTID
                </div>
                <a href="" class="btn btn-dark icon-btn" style="text-align: right"><i class="icon-add"></i>
                    <div class="btn-title">Create Order</div>
                </a>
                <a href="" class="btn btn-light btn-delete"><img src="{{ asset('images/Office_(51).png') }}" alt=""></a>
            </div>
            <div class="row">
                <div class="col-lg-6">
                    <div class="search"><a href="#" class="search_icon"><i class="fa fa-search"></i></a><input
                            class="search_input" type="text" name="" placeholder="Search here..."></div>
                    <div class="redirect">
                        <div class="redirect-header">
                            Category A
                        </div>
                        <div class="redirect-body">
                            <input class="input" type="text" name="" value="Product Item">
                            <input class="input" type="text" name="" value="Product Item">
                            <input class="input" type="text" name="" value="Product Item">
                        </div>
                    </div>

                    <div class="redirect">
                        <div class="redirect-header">
                            Category B
                        </div>
                        <div class="redirect-body">
                            <input class="input" type="text" name="" value="Product Item">
                            <input class="input" type="text" name="" value="Product Item">
                            <input class="input" type="text" name="" value="Product Item">
                        </div>
                    </div>

                    <div class="redirect">
                        <div class="redirect-header">
                            Category B
                        </div>
                        <div class="redirect-body">
                            <input class="input" type="text" name="" value="Product Item">
                            <input class="input" type="text" name="" value="Product Item">
                            <input class="input" type="text" name="" value="Product Item">
                        </div>
                    </div>
                </div>
                <div class="col-lg-6">
                    <div class="product">
                        <div class="product-header">
                            Product List
                        </div>
                        <div class="product-body">
                        </div>
                    </div>
                    <div class="customer">
                        <div class="customer-header">
                            Customer
                        </div>
                        <div class="customer-body">
                        </div>
                    </div>
                    <div class="promotion">
                        <div class="promotion-header">
                            Promotion
                        </div>
                        <div class="promotion-body">
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
