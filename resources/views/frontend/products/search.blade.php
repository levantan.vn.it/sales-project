@extends('layouts.frontend.app')
@section('content')
<style>
    .navFooter{display: none;}
</style>
<div class="layout ">
    <div class="title back" >
        <a href="{{route('product')}}">
            <button class="turn_back">
                <img src="{{asset('css/frontend/myimg/angle.png')}}" alt="">
            </button>
        </a>
        <h1>SEARCH <span>PRODUCTS</span></h1>
    </div>
    <form action="{{route('product.search')}}">
        <div class="search_form">
            <div class="search_box">
                <input type="text" name="" id="" placeholder="Search branchs">
                <a href="{{route('product.search')}}"><img src="{{asset('css/frontend/myimg/lock.png')}}" alt=""></a>
            </div>
            <button class="find"><img src="{{asset('css/frontend/myimg/caledar.png')}}" alt=""></button>
            <button class="find" id="gas1"><img src="{{asset('css/frontend/myimg/fill.png')}}" alt=""></button>
        </div>
    </form>
    <div class="product_box row">
        <div class="product_box_object col-md-3">
            <a href="{{route('branch')}}">
                <div class="object">
                    <h1>Brand Item</h1>
                </div>
            </a>
        </div>
        <div class="product_box_object col-md-3">
            <a href="{{route('branch')}}">
                <div class="object">
                    <h1>Brand Item</h1>
                </div>
            </a>
        </div>
        <div class="product_box_object col-md-3">
            <a href="{{route('branch')}}">
                <div class="object">
                    <h1>Brand Item</h1>
                </div>
            </a>
        </div>
        <div class="product_box_object col-md-3">
            <a href="{{route('branch')}}">
                <div class="object">
                    <h1>Brand Item</h1>
                </div>
            </a>
        </div>
        <div class="product_box_object col-md-3">
            <a href="{{route('branch')}}">
                <div class="object">
                    <h1>Brand Item</h1>
                </div>
            </a>
        </div>
        <div class="product_box_object col-md-3">
            <a href="{{route('branch')}}">
                <div class="object">
                    <h1>Brand Item</h1>
                </div>
            </a>
        </div>
        <div class="product_box_object col-md-3">
            <a href="{{route('branch')}}">
                <div class="object">
                    <h1>Brand Item</h1>
                </div>
            </a>
        </div>
        <div class="product_box_object col-md-3">
            <a href="{{route('branch')}}">
                <div class="object">
                    <h1>Brand Item</h1>
                </div>
            </a>
        </div>
        <div class="product_box_object col-md-3">
            <a href="{{route('branch')}}">
                <div class="object">
                    <h1>Brand Item</h1>
                </div>
            </a>
        </div>
        <div class="product_box_object col-md-3">
            <a href="{{route('branch')}}">
                <div class="object">
                    <h1>Brand Item</h1>
                </div>
            </a>
        </div>
        <div class="product_box_object col-md-3">
            <a href="{{route('branch')}}">
                <div class="object">
                    <h1>Brand Item</h1>
                </div>
            </a>
        </div>
        <div class="product_box_object col-md-3">
            <a href="{{route('branch')}}">
                <div class="object">
                    <h1>Brand Item</h1>
                </div>
            </a>
        </div>
    </div>
</div>
    
@endsection