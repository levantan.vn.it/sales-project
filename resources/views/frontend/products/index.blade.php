@extends('layouts.frontend.app')
@section('content')
    
<div class="layout">
    <div class="title">
        <h1>MY <span>WISHED</span> PRODUCTS</h1>
    </div>
    <form action="{{route('product.search')}}">
        <div class="search_form">
            <div class="search_box">
                <input type="text" name="" id="" placeholder="Search">
                <a href="{{route('product.search')}}"><img src="{{asset('css/frontend/myimg/lock.png')}}" alt=""></a>
            </div>
            <button class="find"><img src="{{asset('css/frontend/myimg/caledar.png')}}" alt=""></button>
            <button class="find" id="gas"><img src="{{asset('css/frontend/myimg/fill.png')}}" alt=""></button>
            <button type="button" class="add_new">
                <a href="{{route('product.create')}}">
                    <img style="display: none" src="{{asset('css/frontend/myimg/sumM.png')}}" alt="">
                    <span>Add New Product</span>
                </a>
            </button>
        </div>
    </form>
    <div class="product_box row">
        <div class="product_box_object col-md-4">
            <a href="{{route('product.detail')}}">
                <div class="object">
                    <h1>Product Item</h1>
                </div>
            </a>
        </div>
        <div class="product_box_object col-md-4">
            <a href="{{route('product.detail')}}">
                <div class="object">
                    <h1>Product Item</h1>
                </div>
            </a>
        </div>
        <div class="product_box_object col-md-4">
            <a href="{{route('product.detail')}}">
                <div class="object">
                    <h1>Product Item</h1>
                </div>
            </a>
        </div>
        <div class="product_box_object col-md-4">
            <a href="{{route('product.detail')}}">
                <div class="object">
                    <h1>Product Item</h1>
                </div>
            </a>
        </div>
        <div class="product_box_object col-md-4">
            <a href="{{route('product.detail')}}">
                <div class="object">
                    <h1>Product Item</h1>
                </div>
            </a>
        </div>
        <div class="product_box_object col-md-4">
            <a href="{{route('product.detail')}}">
                <div class="object">
                    <h1>Product Item</h1>
                </div>
            </a>
        </div>
        <div class="product_box_object col-md-4">
            <a href="{{route('product.detail')}}">
                <div class="object">
                    <h1>Product Item</h1>
                </div>
            </a>
        </div>
        <div class="product_box_object col-md-4">
            <a href="{{route('product.detail')}}">
                <div class="object">
                    <h1>Product Item</h1>
                </div>
            </a>
        </div>
        <div class="product_box_object col-md-4">
            <a href="{{route('product.detail')}}">
                <div class="object">
                    <h1>Product Item</h1>
                </div>
            </a>
        </div>
    </div>
</div>
    
@endsection