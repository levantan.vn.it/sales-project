@extends('layouts.frontend.app')
@section('content')
<style>
    .navFooter{display: none;}
</style>
<div class="layout create">
    <div class="title detail" id="create">
        <div class="back-title">
            <a href="{{route('product')}}">
                <button class="turn_back">
                    <img src="{{asset('css/frontend/myimg/angle.png')}}" alt="">
                </button>
            </a>
            <h1>#PROD<span>UCT</span>ID</h1>
            <button class="status_header"><h5>Status</h5></button>
        </div>
        <div class="trash">
            <button class="add_product"> <img src="{{asset('css/frontend/myimg/sum.png')}}" alt=""><span>ADD THIS PRODUCT</span></button>
        </div>
    </div>
    <div class="product_boxid row">
        <div class="box_site col-md-6 ">
            <div class="product_boxid_object col-md-12">
                <div class="object_title" style="height: 300px">
                    <h1>Product Information</h1>
                </div>
            </div>
            <div class="product_boxid_object col-md-12">
                <div class="object_title" style="height: 200px">
                    <h1>Brand Information</h1>
                </div>
            </div>
        </div>
        <div class="box_site col-md-6 ">
            <div class="product_boxid_object col-md-12">
                <div class="object_title" style="height: 510px">
                    <h1>Selling Policy</h1>
                </div>
            </div>
        </div>
    </div>
</div>
    
@endsection