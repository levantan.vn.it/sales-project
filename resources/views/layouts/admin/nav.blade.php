<!-- Sidebar navigation-->
<nav class="sidebar-nav">
    <ul id="sidebarnav">
        <li>
            <a href="{{ route('admin.dashboard') }}">
                <i class="fa fa-th-large"></i>
                <span class="hide-menu">Dashboard</span>
            </a>
        </li>
        <li>
            <a href="{{ route('sellers.index') }}" @if (isset($page) && $page == 'sellers') class="active"  @endif>
                <i class="mdi mdi-account"></i>
                <span class="hide-menu">Sellers</span>
            </a>
        </li>
        <li>
            <a href="{{ route('brands.index') }}" @if (isset($page) && $page == 'brands') class="active"  @endif>
                <i class="mdi mdi-account-star"></i>
                <span class="hide-menu">Brands</span>
            </a>
        </li>
        <li>
            <a href="{{ route('products.index') }}" @if (isset($page) && $page == 'products') class="active"  @endif>
                <i class="mdi mdi-cube"></i>
                <span class="hide-menu">Products</span>
            </a>
        </li>
        <li>
            <a href="{{ route('promotions.index') }}" @if (isset($page) && $page == 'promotions') class="active"  @endif>
                <i class="mdi mdi-ticket-percent"></i>
                <span class="hide-menu">Promotions</span>
            </a>
        </li>
        <li>
            <a href="{{ route('orders.index') }}" @if (isset($page) && $page == 'orders') class="active"  @endif>
                <i class="mdi mdi-dropbox"></i>
                <span class="hide-menu">Orders</span>
            </a>
        </li>
        <li>
            <a href="{{ route('settles.index') }}" @if (isset($page) && $page == 'settles') class="active"  @endif>
                <i class="ti-settings"></i>
                <span class="hide-menu">Settles</span>
            </a>
        </li>
        <li>
            <a href="{{ route('admin.settings') }}" @if (isset($page) && $page == 'settings') class="active"  @endif>
                <i class="ti-settings"></i>
                <span class="hide-menu">Settings</span>
            </a>
        </li>
        <li>
            <a href="{{ route('users.index') }}" @if (isset($page) && $page == 'users') class="active"  @endif>
                <i class="mdi mdi-account-settings-variant"></i>
                <span class="hide-menu">Admin users</span>
            </a>
        </li>
        <li>
            <a class="waves-effect waves-dark" href="javascript:void(0)" aria-expanded="false" id="logout-confirm">
                <i class="fas fa-sign-out-alt"></i><span class="hide-menu">Logout</span>
            </a>
        </li>
    </ul>
</nav>
<!-- End Sidebar navigation -->
