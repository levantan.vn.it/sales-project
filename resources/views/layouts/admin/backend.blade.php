<!DOCTYPE html>
<html lang="{{ app()->getLocale() }}">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>C2C Sale Support Platform</title>

    <meta name="csrf-token" content="{{ csrf_token() }}">

    <!-- Scripts -->
{{--    <script src="{{ asset('js/backend.js') }}" defer></script>--}}

    <link href="{{ asset('css/admin/style.css') }}?v={{ uniqid() }}" rel="stylesheet">
    <link href="{{ asset('css/admin/custom.css') }}?v={{ uniqid() }}" rel="stylesheet">

    <link href="{{ asset('css/admin/media/dataTables.bootstrap.min.css?v=' . time()) }}" rel="stylesheet" type="text/css" />
{{--    <link href="{{ asset('css/admin/table.css?v=' . time()) }}" rel="stylesheet" type="text/css" />--}}


    @stack('custom-css')
</head>
<body class="skin-blue fixed-layout">
    <div id="app">

        <div id="main-wrapper">
            <header class="topbar">
                <nav class="navbar top-navbar navbar-expand-md navbar-dark">
                    <div class="navbar-header">
                        <a class="navbar-brand" href="{{ route('admin.dashboard') }}">
                            <b style="display: none;">
                                <img src="/images/logo.png" class="light-logo" alt="logo" />
                            </b>
                            <span>
                                <img src="/images/logo-text.png" class="light-logo" alt="logo" />
                            </span>
                        </a>
                    </div>
                    <div class="navbar-collapse">
                        <ul class="navbar-nav mr-auto">
                            <!-- This is  -->
                            <li class="nav-item"> <a class="nav-link nav-toggler d-block d-md-none waves-effect waves-dark" href="javascript:void(0)">
                                    <i class="ti-menu"></i></a>
                            </li>
                            <li class="nav-item"> <a class="nav-link sidebartoggler d-none d-lg-block d-md-block waves-effect waves-dark" href="javascript:void(0)">
                                    <i class="icon-menu"></i></a>
                            </li>
                        </ul>


                        <ul class="navbar-nav my-lg-0">
                            <li class="nav-item dropdown u-pro">
                                <a class="nav-link dropdown-toggle waves-effect waves-dark profile-pic" href="" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                    <img src="{{ (Auth::user()->image) ? asset('storage/' . Auth::user()->image->path) : asset('images/default_user_icon.png') }}" alt="user" class="">
                                    <span class="hidden-md-down">{{ Auth::user()->name }} &nbsp;<!--<i class="fa fa-angle-down"></i>--></span>
                                </a>
                                <div class="dropdown-menu dropdown-menu-right animated flipInY">
                                    <!-- text-->
{{--                                    <a href="javascript:void(0)" class="dropdown-item"><i class="ti-user"></i> My Profile</a>--}}
{{--                                    <!-- text-->--}}
{{--                                    <a href="javascript:void(0)" class="dropdown-item"><i class="ti-wallet"></i> My Balance</a>--}}
{{--                                    <!-- text-->--}}
{{--                                    <a href="javascript:void(0)" class="dropdown-item"><i class="ti-email"></i> Inbox</a>--}}
{{--                                    <!-- text-->--}}
{{--                                    <div class="dropdown-divider"></div>--}}
{{--                                    <!-- text-->--}}
{{--                                    <a href="javascript:void(0)" class="dropdown-item"><i class="ti-settings"></i> Account Setting</a>--}}
{{--                                    <!-- text-->--}}
{{--                                    <div class="dropdown-divider"></div>--}}
                                    <!-- text-->
                                    <a href="{{ route('admin.logout') }}" class="dropdown-item"><i class="fa fa-power-off"></i> Logout</a>
                                    <!-- text-->
                                </div>
                            </li>
                            <!-- ============================================================== -->
                            <!-- End User Profile -->
                            <!-- ============================================================== -->
                        </ul>
                    </div>
                </nav>
            </header>

            <aside class="left-sidebar">
                <!-- Sidebar scroll-->
                <div class="scroll-sidebar">
                    @include('layouts.admin.nav')
                </div>
                <!-- End Sidebar scroll-->
            </aside>
            <div class="page-wrapper">
                <div class="container-fluid">

                    @yield('content')

                </div>

            </div>
        </div>
    </div>

    {{-- Laravel Mix - JS File --}}
    <script type="text/javascript" src="{{ asset('vendors/js/jquery-3.2.1.min.js')}}?v={{ uniqid() }}"></script>
    <script type="text/javascript" src="{{ asset('vendors/js/popper.min.js')}}?v={{ uniqid() }}"></script>
    <script type="text/javascript" src="{{ asset('vendors/js/bootstrap.min.js')}}?v={{ uniqid() }}"></script>
    <script type="text/javascript" src="{{ asset('vendors/js/bootstrap-input-spinner.js')}}?v={{ uniqid() }}"></script>
    <script type="text/javascript" src="{{ asset('vendors/js/sidebarmenu.js')}}?v={{ uniqid() }}"></script>
    <script type="text/javascript" src="{{ asset('vendors/js/custom.js')}}?v={{ uniqid() }}"></script>
    <script type="text/javascript" src="{{ asset('vendors/js/jquery-ui.min.js')}}?v={{ uniqid() }}"></script>
{{--    <script type="text/javascript" src="{{ asset('js/main.js')}}"></script>--}}

    <script src="{{ asset('vendors/js/datatables/media/js/jquery.dataTables.min.js?v=' . time()) }}"></script>
    <script src="{{ asset('vendors/js/datatables/media/js/dataTables.bootstrap.min.js?v=' . time()) }}"></script>
    <script src="{{ asset('vendors/js/table/table.js?v=' . time()) }}"></script>


    <script type="text/javascript" src="{{ asset('vendors/switchery/dist/switchery.min.js') }}?v={{ uniqid() }}"></script>

    <!-- Sweet-Alert  -->
    <script src="{{ asset('vendors/js/sweetalert2.all.min.js') }}?v={{ uniqid() }}"></script>
    <script type="text/javascript">
        $(function() {
            $("#logout-confirm").click(function () {
                Swal.fire({
                    title: 'Are you sure?',
                    text: "You want to logout.",
                    type: 'warning',
                    showCancelButton: true,
                    confirmButtonColor: '#3085d6',
                    cancelButtonColor: '#d33',
                    confirmButtonText: 'Yes'
                }).then((result) => {
                    if (result.value) {
                        window.location = '/admin/logout';
                    }
                })
            });
        });

    </script>
    @yield('page-script')
    @stack('javascript')
</body>
</html>
