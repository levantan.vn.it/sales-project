<footer>
    <div class="navFooter row">
        <div class="object col">
            <a href="{{route('dashboard')}}">
                <img class="{{ request()->is('dashboard') ? 'active' : '' }}" src="{{asset('css/frontend/myimg/dash.png')}}" alt="">
            </a>
        </div>
        <div class="object col">
            <a href="">
                <img class="{{ request()->is('order') ? 'active' : '' }}" src="{{asset('css/frontend/myimg/box.png')}}" alt="">
                <button class="notify"></button>
            </a>
        </div>
        <div class="object col">
            <a href="">
                <img class="{{ request()->is('customer') ? 'active' : '' }}" src="{{asset('css/frontend/myimg/star.png')}}" alt="">
            </a>
        </div>
        <div class="object col">
            <a href="{{route('product')}}">
                <img class="{{ request()->is('product') ? 'active' : '' }}" src="{{asset('css/frontend/myimg/chair.png')}}" alt="">
            </a>
        </div>
        <div class="object col">
            <a href="{{route('home')}}">
                <img class="{{ request()->is('/') ? 'active' : '' }}" src="{{asset('css/frontend/myimg/dots.png')}}" alt="">
            </a>
        </div>
    </div>
</footer>