<div class="siderbar">
    <a href="{{route('home')}}">
        <button class="redirect {{ request()->is('/') ? 'active' : '' }}">
            <img class="dot {{ request()->is('/') ? 'active' : '' }}" src="{{asset('css/frontend/myimg/home.png')}}" alt="">
        </button>
    </a>
    <a href="{{route('dashboard')}}">
        <button class="redirect {{ request()->is('dashboard') ? 'active' : '' }}">
            <img class="dot {{ request()->is('dashboard') ? 'active' : '' }}" src="{{asset('css/frontend/myimg/dash.png')}}" alt="">
        </button>
    </a>
    <a href="">
    <button class="redirect {{ request()->is('') ? 'active' : '' }}" id="own">
        <img class="dot" {{ request()->is('') ? 'active' : '' }} src="{{asset('css/frontend/myimg/box.png')}}" alt="">
        <button class="slave">4</button>
    </button>
    </a>
    <a href="">
        <button class="redirect {{ request()->is('') ? 'active' : '' }}">
            <img class="dot {{ request()->is('') ? 'active' : '' }}" src="{{asset('css/frontend/myimg/star.png')}}" alt="">
        </button>
    </a>
    <a href="{{route('product')}}">
        <button class="redirect {{ request()->is('product') ? 'active' : '' }}">
            <img class="dot {{ request()->is('product') ? 'active' : '' }}" src="{{asset('css/frontend/myimg/chair.png')}}" alt="">
        </button>
    </a>
    <a href="{{route('promotion')}}">
        <button class="redirect {{ request()->is('promotion') ? 'active' : '' }}">
            <img class="dot {{ request()->is('promotion') ? 'active' : '' }}" src="{{asset('css/frontend/myimg/ticker.png')}}" alt="">
        </button>
    </a>
    <a href="">
        <button class="redirect {{ request()->is('') ? 'active' : '' }}">
            <img class="dot {{ request()->is('') ? 'active' : '' }}" src="{{asset('css/frontend/myimg/list.png')}}" alt="">
        </button>
    </a>
    <a href="">
        <button class="redirect {{ request()->is('') ? 'active' : '' }}">
            <img class="dot {{ request()->is('') ? 'active' : '' }}" src="{{asset('css/frontend/myimg/setting.png')}}" alt="">
        </button>
    </a>
</div>
