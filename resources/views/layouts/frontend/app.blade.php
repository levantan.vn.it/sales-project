<!DOCTYPE html>
<html lang="{{ app()->getLocale() }}">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>C2C Sale Support Platform</title>

    <meta name="csrf-token" content="{{ csrf_token() }}">

    <!-- Scripts -->
{{--    <script src="{{ asset('js/backend.js') }}" defer></script>--}}

    <link href="{{ asset('css/admin/style.css') }}?v={{ uniqid() }}" rel="stylesheet">
    <link href="{{ asset('css/frontend/style.css') }}?v={{ uniqid() }}" rel="stylesheet">
    <link href="{{asset('css/frontend/mycss/style.css?v='.time())}}" rel="stylesheet" />

    <link href="{{ asset('css/admin/media/dataTables.bootstrap.min.css?v=' . time()) }}" rel="stylesheet" type="text/css" />
{{--    <link href="{{ asset('css/admin/table.css?v=' . time()) }}" rel="stylesheet" type="text/css" />--}}
    <link rel="stylesheet" href="{{ asset('css/frontend/mystyle.css') }}">


    @stack('custom-css')
</head>
<body class="skin-blue fixed-layout">
    <div id="app">

        <div id="main-wrapper">
            <div class="header">
                <div class="left_header">
                    <h1 class="header_name">C2C SSP</h1>
                </div>
                <div class="right_header">
                    <button class="avatar" id="bell">
                        <img src="{{asset('css/frontend/myimg/Vector.png')}}" alt="">
                        <img class="red" src="{{asset('css/frontend/myimg/Rectangle 2.25.png')}}" alt="">
                    </button>
                    <button class="avatar">
                        <img src="{{asset('css/frontend/myimg/Member.png')}}"alt="">
                    </button>
                </div>
            </div>

            <aside class="left-sidebar">
                <!-- Sidebar scroll-->
                <div class="scroll-sidebar">
                    @include('layouts.frontend.nav')
                </div>
                <!-- End Sidebar scroll-->
            </aside>
            <div class="page-wrapper">
                <div class="container-fluid">

                    @yield('content')

                </div>
            </div>
            @include('layouts.frontend.navFooter')
        </div>
    </div>

    {{-- Laravel Mix - JS File --}}
    <script type="text/javascript" src="{{ asset('vendors/js/jquery-3.2.1.min.js')}}?v={{ uniqid() }}"></script>
    <script type="text/javascript" src="{{ asset('vendors/js/popper.min.js')}}?v={{ uniqid() }}"></script>
    <script type="text/javascript" src="{{ asset('vendors/js/bootstrap.min.js')}}?v={{ uniqid() }}"></script>
    <script type="text/javascript" src="{{ asset('vendors/js/bootstrap-input-spinner.js')}}?v={{ uniqid() }}"></script>
    <script type="text/javascript" src="{{ asset('vendors/js/sidebarmenu.js')}}?v={{ uniqid() }}"></script>
    <script type="text/javascript" src="{{ asset('vendors/js/custom.js')}}?v={{ uniqid() }}"></script>
    <script type="text/javascript" src="{{ asset('vendors/js/jquery-ui.min.js')}}?v={{ uniqid() }}"></script>
{{--    <script type="text/javascript" src="{{ asset('js/main.js')}}"></script>--}}

    <script src="{{ asset('vendors/js/datatables/media/js/jquery.dataTables.min.js?v=' . time()) }}"></script>
    <script src="{{ asset('vendors/js/datatables/media/js/dataTables.bootstrap.min.js?v=' . time()) }}"></script>
    <script src="{{ asset('vendors/js/table/table.js?v=' . time()) }}"></script>


    <script type="text/javascript" src="{{ asset('vendors/switchery/dist/switchery.min.js') }}?v={{ uniqid() }}"></script>

    <!-- Sweet-Alert  -->
    <script src="{{ asset('vendors/js/sweetalert2.all.min.js') }}?v={{ uniqid() }}"></script>
    <script type="text/javascript">
        $(function() {
            $("#logout-confirm").click(function () {
                Swal.fire({
                    title: 'Are you sure?',
                    text: "You want to logout.",
                    type: 'warning',
                    showCancelButton: true,
                    confirmButtonColor: '#3085d6',
                    cancelButtonColor: '#d33',
                    confirmButtonText: 'Yes'
                }).then((result) => {
                    if (result.value) {
                        window.location = '/admin/logout';
                    }
                })
            });
        });

    </script>
    @yield('page-script')
    @stack('javascript')
</body>
</html>
