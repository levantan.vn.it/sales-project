@extends('layouts.admin.backend', ['page' => 'sellers'])

@section('content')
    <div class="row page-titles">
        <div class="col-md-5 align-self-center">
            <h4 class="text-themecolor">Sellers</h4>
        </div>
    </div>

    <div class="card">
        <div class="card-body">
            <div class="row p-t-20">
                <div class="col-md-6">
                    <div class="form-group">
                        <input type="text" id="search" class="form-control" placeholder="Search">
                    </div>
                </div>
                <div class="col-md-6 row">
                    <div class="col">
                        <div class="form-group">
                            <select class="form-control custom-select" data-placeholder="Filter">
                                <option value="1">Filter</option>
                            </select>
                        </div>
                    </div>
                    <div class="col">
                        <div class="form-group">
                            <select class="form-control custom-select" data-placeholder="Filter">
                                <option value="1">Filter</option>
                            </select>
                        </div>
                    </div>

                </div>
            </div>
            <div class="row p-t-20">
                <div class="col-md-9 row">
                    <div class="col">
                        <div class="form-group">
                            <input class="form-control" type="date" value="2021-08-16" id="example-date-input">
                        </div>
                    </div>
                    <div class="col">
                        <div class="form-group">
                            <input class="form-control" type="date" value="2021-08-16" id="example-date-input">
                        </div>
                    </div>
                    <div class="col">
                        <div class="form-group">
                            <select class="form-control custom-select" data-placeholder="Filter">
                                <option value="1">Filter</option>
                            </select>
                        </div>
                    </div>
                </div>
                <div class="col-md-3 row">
                    <div class="col">
                        <div class="form-group">
                            <button type="submit" class="btn btn-success mr-2">Search</button>
                            <button type="submit" class="btn btn-dark mr-2">Reset</button>
                            <button type="submit" class="btn btn-primary mr-2">Export</button>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div class="card">
        <div class="card-body">
            <div class="table-responsive">
                <table class="table">
                    <thead>
                    <tr>
                        <th>#</th>
                        <th>TITLE</th>
                        <th>TITLE</th>
                        <th>TITLE</th>
                        <th>TITLE</th>
                        <th>TITLE</th>
                        <th>TITLE</th>
                        <th>TITLE</th>
                    </tr>
                    </thead>
                    <tbody>
                    <tr>
                        <td>1</td>
                        <td>
                            <a href="{{ route('sellers.show', 1) }}">Value</a>
                        </td>
                        <td>Value</td>
                        <td>Value</td>
                        <td>Value</td>
                        <td>Value</td>
                        <td>Value</td>
                        <td>Value</td>
                    </tr>
                    <tr>
                        <td>2</td>
                        <td>
                            <a href="{{ route('sellers.show', 2) }}">Value</a>
                        </td>
                        <td>Value</td>
                        <td>Value</td>
                        <td>Value</td>
                        <td>Value</td>
                        <td>Value</td>
                        <td>Value</td>
                    </tr>
                    <tr>
                        <td>3</td>
                        <td>
                            <a href="{{ route('sellers.show', 3) }}">Value</a>
                        </td>
                        <td>Value</td>
                        <td>Value</td>
                        <td>Value</td>
                        <td>Value</td>
                        <td>Value</td>
                        <td>Value</td>
                    </tr>
                    <tr>
                        <td>4</td>
                        <td>
                            <a href="{{ route('sellers.show', 4) }}">Value</a>
                        </td>
                        <td>Value</td>
                        <td>Value</td>
                        <td>Value</td>
                        <td>Value</td>
                        <td>Value</td>
                        <td>Value</td>
                    </tr>
                    <tr>
                        <td>5</td>
                        <td>
                            <a href="{{ route('sellers.show', 5) }}">Value</a>
                        </td>
                        <td>Value</td>
                        <td>Value</td>
                        <td>Value</td>
                        <td>Value</td>
                        <td>Value</td>
                        <td>Value</td>
                    </tr>
                    <tr>
                        <td>6</td>
                        <td>
                            <a href="{{ route('sellers.show', 6) }}">Value</a>
                        </td>
                        <td>Value</td>
                        <td>Value</td>
                        <td>Value</td>
                        <td>Value</td>
                        <td>Value</td>
                        <td>Value</td>
                    </tr>
                    </tbody>
                </table>
                <div class="dataTables_info" id="example23_info" role="status" aria-live="polite">Showing 1 to 10 of 57 entries</div>
                <div class="dataTables_paginate paging_simple_numbers" id="example23_paginate" style="float: right;">
                    <ul class="pagination">
                        <li class="paginate_button page-item previous disabled" id="example23_previous"><a href="#" aria-controls="example23" data-dt-idx="0" tabindex="0" class="page-link">Previous</a></li><li class="paginate_button page-item active"><a href="#" aria-controls="example23" data-dt-idx="1" tabindex="0" class="page-link">1</a></li><li class="paginate_button page-item "><a href="#" aria-controls="example23" data-dt-idx="2" tabindex="0" class="page-link">2</a></li><li class="paginate_button page-item "><a href="#" aria-controls="example23" data-dt-idx="3" tabindex="0" class="page-link">3</a></li><li class="paginate_button page-item "><a href="#" aria-controls="example23" data-dt-idx="4" tabindex="0" class="page-link">4</a></li><li class="paginate_button page-item "><a href="#" aria-controls="example23" data-dt-idx="5" tabindex="0" class="page-link">5</a></li><li class="paginate_button page-item "><a href="#" aria-controls="example23" data-dt-idx="6" tabindex="0" class="page-link">6</a></li><li class="paginate_button page-item next" id="example23_next"><a href="#" aria-controls="example23" data-dt-idx="7" tabindex="0" class="page-link">Next</a></li>
                    </ul>
                </div>
            </div>
        </div>
    </div>

@endsection
