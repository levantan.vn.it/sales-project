<div class="table-actions">

    <a href="{{ route('users.edit', $item->id) }}" class="btn btn-icon btn-sm btn-primary" data-toggle="tooltip" data-original-title="edit"><i class="fa fa-edit"></i></a>

    <a href="#" class="btn btn-icon btn-sm btn-danger deleteDialog" data-toggle="tooltip" data-section="{{ route('users.destroy', $item->id) }}" role="button" data-original-title="delete" >
        <i class="fa fa-trash"></i>
    </a>
</div>
