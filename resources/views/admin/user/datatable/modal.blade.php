<div class="modal fade modal-confirm-delete" tabindex="-1" role="dialog">
    <div class="modal-dialog modal-sm">
        <div class="modal-content">
            <div class="modal-header bg-danger">
                <h4 class="modal-title"><i class="til_img"></i><strong>Confirm delete</strong></h4>
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">
                    <span aria-hidden="true">×</span>
                </button>
            </div>

            <div class="modal-body with-padding">
                <div>Do you really want to delete this record?</div>
            </div>

            <div class="modal-footer">
                <button class="float-left btn btn-warning" data-dismiss="modal">Cancel</button>
                <button class="float-right btn btn-danger delete-crud-entry">Delete</button>
            </div>
        </div>
    </div>
</div>
<!-- end Modal -->
