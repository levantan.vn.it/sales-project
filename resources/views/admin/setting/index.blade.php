@extends('layouts.admin.backend', ['page' => 'settings'])

@section('content')
    <div class="row page-titles">
        <div class="col-md-5 align-self-center">
            <h4 class="text-themecolor">Settings</h4>
        </div>
    </div>

    <div class="row">
        <div class="col-2">
            <div class="card">
                <div class="card-body" style="height: 200px;">

                </div>
            </div>
        </div>
        <div class="col-2">
            <div class="card">
                <div class="card-body" style="height: 200px;">
                </div>
            </div>
        </div>
        <div class="col-2">
            <div class="card">
                <div class="card-body" style="height: 200px;">

                </div>
            </div>
        </div>
        <div class="col-2">
            <div class="card">
                <div class="card-body" style="height: 200px;">
                </div>
            </div>
        </div>
    </div>

@endsection
