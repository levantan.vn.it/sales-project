@extends('layouts.admin.backend', ['page' => 'sellers'])

@section('content')
    <div class="row page-titles">
        <div class="col-md-5 align-self-center">
            <h4 class="text-themecolor">Sellers</h4>
        </div>
    </div>

    <div class="card">
        <div class="card-body row">
            <div class="col-1">
                <h4 class="card-title">#ProductID</h4>
                <h6 class="card-subtitle">Product Title</h6>
            </div>
            <div class="col-3">
                <button type="button" class="btn btn-outline-primary btn-rounded">Status</button>
            </div>
        </div>
    </div>

    <div class="row">
        <div class="col-7">
            <div class="card">
                <div class="card-body" style="height: 300px;">
                    <h4 class="card-title">Product Information</h4>
                </div>
            </div>
        </div>
        <div class="col-5">
            <div class="card">
                <div class="card-body" style="height: 300px;">
                    <h4 class="card-title">Seller List</h4>
                </div>
            </div>
        </div>
    </div>

@endsection
