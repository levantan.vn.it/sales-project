<?php

use Illuminate\Support\Facades\Route;

Route::namespace('Admin')->group(function () {

    Route::redirect('admin', 'admin/login');
    Route::get('admin/login', 'AdminLoginController@showLoginForm')->name('admin.login');
    Route::post('admin/login', 'AdminLoginController@login')->name('admin.login');
    Route::get('admin/logout', 'AdminLoginController@logout')->name('admin.logout');

    Route::group(['middleware' => ['auth:admin']], function () {
        Route::get('admin/dashboard', 'DashboardController@index')->name('admin.dashboard');

        Route::resource('admin/users', AdminUserController::class);

        Route::resource('admin/sellers', SellerController::class);

        Route::resource('admin/brands', BrandController::class);

        Route::resource('admin/products', ProductController::class);

        Route::resource('admin/promotions', PromotionController::class);

        Route::resource('admin/orders', OrderController::class);

        Route::resource('admin/settles', SettleController::class);

        Route::get('admin/settings', 'SettingController@index')->name('admin.settings');

    });
});
