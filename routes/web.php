<?php

use Illuminate\Support\Facades\Route;
use Illuminate\Support\Facades\Auth;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/
Auth::routes();
Route::group(['middleware' => []], function () {
    Route::get('/', 'DashboardController@home')->name('home');
    Route::get('/dashboard', 'DashboardController@dashboard')->name('dashboard');
    // Product  
    Route::get('/product', 'DashboardController@product')->name('product');
    Route::get('/product/search', 'DashboardController@productSearch')->name('product.search');
    Route::get('/product/detail', 'DashboardController@productDetail')->name('product.detail');
    Route::get('/product/create', 'DashboardController@productCreate')->name('product.create');

    // Branch
    Route::get('/branch', 'DashboardController@branch')->name('branch');

    // Promotion
    Route::get('/promotion', 'DashboardController@promotion')->name('promotion');
    Route::get('/promotion/detail', 'DashboardController@promotionDetail')->name('promotion.detail');


});

Route::get('/cart', function () {
    return view('frontend.cart.index');
});
Route::get('/cart/detail', function () {
    return view('frontend.cart.detail');
});

Route::get('/order', function () {
    return view('frontend.order.index');
});
Route::get('/order/detail', function () {
    return view('frontend.order.detail');
});

Route::get('/delivery', function () {
    return view('frontend.delivery.index');
});
Route::get('/delivery/detail', function () {
    return view('frontend.delivery.detail');
});
